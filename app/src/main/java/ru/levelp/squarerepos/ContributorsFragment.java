package ru.levelp.squarerepos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContributorsFragment extends Fragment {
  private static final String TAG = ContributorsFragment.class.getSimpleName();

  private RecyclerView recyclerView;

  public ContributorsFragment() {}

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_contributors, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    recyclerView = (RecyclerView) view;
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
  }

  public void setRepo(String repo) {
    SquareReposApplication app = (SquareReposApplication) getContext().getApplicationContext();
    GitHubService gitHubService = app.getGitHubService();
    gitHubService.contributors("square", repo).enqueue(new Callback<List<Contributor>>() {
      @Override
      public void onResponse(Call<List<Contributor>> call, Response<List<Contributor>> response) {
        if (response.isSuccessful()) {
          List<Contributor> contributors = response.body();
          recyclerView.setAdapter(new ContributorsAdapter(contributors, Glide.with(ContributorsFragment.this)));
        } else {
          Toast.makeText(getContext(), "Failed to load contributors", Toast.LENGTH_LONG).show();
        }
      }

      @Override
      public void onFailure(Call<List<Contributor>> call, Throwable t) {
        Log.e(TAG, "Failed to load contributors", t);
        Toast.makeText(getContext(), "Failed to load contributors", Toast.LENGTH_LONG).show();
      }
    });
  }
}