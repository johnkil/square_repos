package ru.levelp.squarerepos;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

public class CommitsAdapter extends RecyclerView.Adapter<CommitsAdapter.ViewHolder> {
  private final List<Commit> commits;

  public CommitsAdapter(List<Commit> commits) {
    this.commits = commits;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.item_commit, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    Commit commit = commits.get(position);
    holder.bindTo(commit);
  }

  @Override
  public int getItemCount() {
    return commits.size();
  }

  static class ViewHolder extends RecyclerView.ViewHolder {
    private final TextView messageView;

    ViewHolder(View itemView) {
      super(itemView);
      messageView = (TextView) itemView.findViewById(R.id.messageView);
    }

    void bindTo(Commit commit) {
      messageView.setText(commit.getCommitInfo().getMessage());
    }
  }
}