package ru.levelp.squarerepos;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

public class ReposAdapter extends RecyclerView.Adapter<ReposAdapter.ViewHolder> {
  private final List<Repo> repos;
  private final RepoClickListener repoClickListener;

  public ReposAdapter(List<Repo> repos, RepoClickListener repoClickListener) {
    this.repos = repos;
    this.repoClickListener = repoClickListener;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.item_repo, parent, false);
    return new ViewHolder(view, repoClickListener);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    Repo repo = repos.get(position);
    holder.bindTo(repo);
  }

  @Override
  public int getItemCount() {
    return repos.size();
  }

  static class ViewHolder extends RecyclerView.ViewHolder {
    private final TextView nameView;
    private final TextView starsNumView;
    private final TextView forksNumView;

    private Repo repo;

    ViewHolder(View itemView, final RepoClickListener repoClickListener) {
      super(itemView);
      nameView = (TextView) itemView.findViewById(R.id.nameView);
      starsNumView = (TextView) itemView.findViewById(R.id.starsNumView);
      forksNumView = (TextView) itemView.findViewById(R.id.forksNumView);
      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          repoClickListener.onRepoClick(repo);
        }
      });
    }

    void bindTo(Repo repo) {
      this.repo = repo;
      nameView.setText(repo.getName());
      starsNumView.setText(itemView.getResources().getString(R.string.stars_format, repo.getStarsCount()));
      forksNumView.setText(itemView.getResources().getString(R.string.forks_format, repo.getForksCount()));
    }
  }

  public interface RepoClickListener {
    void onRepoClick(Repo repo);
  }
}