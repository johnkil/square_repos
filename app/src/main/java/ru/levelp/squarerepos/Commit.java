package ru.levelp.squarerepos;

public class Commit {
  private CommitInfo commit;

  public Commit(CommitInfo commit) {this.commit = commit;}

  public CommitInfo getCommitInfo() {
    return commit;
  }

  public class CommitInfo {
    private String message;

    private CommitInfo(String message) {this.message = message;}

    public String getMessage() {
      return message;
    }
  }
}