package ru.levelp.squarerepos;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.RequestManager;
import java.util.List;

public class ContributorsAdapter extends RecyclerView.Adapter<ContributorsAdapter.ViewHolder> {
  private final List<Contributor> contributors;
  private final RequestManager imageRequestManager;

  public ContributorsAdapter(List<Contributor> contributors, RequestManager imageRequestManager) {
    this.contributors = contributors;
    this.imageRequestManager = imageRequestManager;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.item_contributor, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    Contributor contributor = contributors.get(position);
    holder.bindTo(contributor);
  }

  @Override
  public int getItemCount() {
    return contributors.size();
  }

  class ViewHolder extends RecyclerView.ViewHolder {
    private final ImageView avatarView;
    private final TextView loginView;

    ViewHolder(View itemView) {
      super(itemView);
      avatarView = (ImageView) itemView.findViewById(R.id.avatarView);
      loginView = (TextView) itemView.findViewById(R.id.loginView);
    }

    void bindTo(Contributor contributor) {
      imageRequestManager.load(contributor.getAvatarUrl()).into(avatarView);
      loginView.setText(contributor.getLogin());
    }
  }
}