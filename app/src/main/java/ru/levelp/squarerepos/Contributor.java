package ru.levelp.squarerepos;

public class Contributor {
  private String login;
  private String avatar_url;

  public Contributor(String login, String avatar_url) {
    this.login = login;
    this.avatar_url = avatar_url;
  }

  public String getLogin() {
    return login;
  }

  public String getAvatarUrl() {
    return avatar_url;
  }
}