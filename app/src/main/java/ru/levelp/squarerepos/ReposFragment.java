package ru.levelp.squarerepos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReposFragment extends Fragment implements ReposAdapter.RepoClickListener {
  private static final String TAG = ReposFragment.class.getSimpleName();

  private RecyclerView recyclerView;

  public ReposFragment() {}

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_repositories, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    recyclerView = (RecyclerView) view;
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

    loadSquareRepos();
  }

  private void loadSquareRepos() {
    SquareReposApplication app = (SquareReposApplication) getContext().getApplicationContext();
    GitHubService gitHubService = app.getGitHubService();
    gitHubService.repos("square").enqueue(new Callback<List<Repo>>() {
      @Override
      public void onResponse(Call<List<Repo>> call, Response<List<Repo>> response) {
        if (response.isSuccessful()) {
          List<Repo> repositories = response.body();
          recyclerView.setAdapter(new ReposAdapter(repositories, ReposFragment.this));
        } else {
          Toast.makeText(getContext(), "Failed to load repos", Toast.LENGTH_LONG).show();
        }
      }

      @Override
      public void onFailure(Call<List<Repo>> call, Throwable t) {
        Log.e(TAG, "Failed to load repos", t);
        Toast.makeText(getContext(), "Failed to load repos", Toast.LENGTH_LONG).show();
      }
    });
  }

  @Override
  public void onRepoClick(Repo repo) {
    RepoInfoActivity.launch(getContext(), repo);
  }
}
