package ru.levelp.squarerepos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class RepoInfoFragment extends Fragment {

  private TextView nameView;
  private TextView descriptionView;
  private Button commitsButton;
  private Button contributorsButton;

  public RepoInfoFragment() {}

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_repo_info, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    nameView = (TextView) view.findViewById(R.id.nameView);
    descriptionView = (TextView) view.findViewById(R.id.descriptionView);
    commitsButton = (Button) view.findViewById(R.id.commitsButton);
    contributorsButton = (Button) view.findViewById(R.id.contributorsButton);
  }

  public void setRepo(final Repo repo) {
    nameView.setText(repo.getName());
    descriptionView.setText(repo.getDescription());
    commitsButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        CommitsActivity.launch(getContext(), repo.getName());
      }
    });
    contributorsButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        ContributorsActivity.launch(getContext(), repo.getName());
      }
    });
  }
}