package ru.levelp.squarerepos;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public class ContributorsActivity extends AppCompatActivity {
  private static final String EXTRA_REPO = "repo";

  public static void launch(Context context, String repo) {
    Intent i = new Intent(context, ContributorsActivity.class);
    i.putExtra(EXTRA_REPO, repo);
    context.startActivity(i);
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_contributors);

    String repo = getIntent().getStringExtra(EXTRA_REPO);
    ContributorsFragment contributorsFragment =
        (ContributorsFragment) getSupportFragmentManager().findFragmentById(R.id.contributorsFragment);
    contributorsFragment.setRepo(repo);
  }
}
