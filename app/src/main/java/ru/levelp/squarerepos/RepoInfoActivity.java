package ru.levelp.squarerepos;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public class RepoInfoActivity extends AppCompatActivity {
  private static final String EXTRA_REPO = "repo";

  public static void launch(Context context, Repo repo) {
    Intent i = new Intent(context, RepoInfoActivity.class);
    i.putExtra(EXTRA_REPO, repo);
    context.startActivity(i);
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_repo_info);

    Repo repo = (Repo) getIntent().getSerializableExtra(EXTRA_REPO);
    RepoInfoFragment repoInfoFragment =
        (RepoInfoFragment) getSupportFragmentManager().findFragmentById(R.id.repoInfoFragment);
    repoInfoFragment.setRepo(repo);
  }
}